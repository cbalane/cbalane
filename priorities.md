---
layout: markdown_page
title: "Chris Balane's Weekly Snippets"
---


## Prior Quarter priorities
<details><summary>Click to expand</summary>
# Quarterly Priorities:
This content is meant to communicate how I intend to allocate my time. It should remain consistent on the time-scales of quarters.

## FY23 Q4 
| Theme | Notes | Percent |
| ------ | ------ | ------ |
| Sensing mechanisms / Discovery Track | Customer interviewing, competition, walkthroughs, KPIs | 30% |
| Build Track | Refine backlog, reach planning 2 milestones ahead | 20% |
| Vision, strategy, roadmap | Develop, iterate on, and share artifacts on a regular basis | 20% |
| Go to market | Build relationships with Sales team and create collateral to help with stage adoption | 15% |
| Building relationships with team | Coffee chats, company syncs, team meetings | 10% |
| Personal Development | Reading, Learning, Training | 5% |

## FY24 Q1 (draft)
| Theme | Notes | Percent |
| ------ | ------ | ------ |
| Sensing mechanisms / Discovery Track | Customer interviewing, competition, walkthroughs, KPIs | 30% |
| Build Track | Refine backlog, reach planning 2 milestones ahead | 20% |
| Vision, strategy, roadmap | Develop, iterate on, and share artifacts on a regular basis | 20% |
| Go to market | Build relationships with Sales team and create collateral to help with stage adoption | 15% |
| Building relationships with team | Coffee chats, company syncs, team meetings | 5% |
| Personal Development | Reading, Learning, Training, Domain knowledge | 10% |

## FY23 Q3 
| Theme | Notes | Percent |
| ------ | ------ | ------ |
| Sensing mechanisms / Discovery Track | Customer interviewing, competition, walkthroughs, KPIs | 30% |
| Build Track | Refine backlog, reach planning 2 milestones ahead | 25% |
| Vision, strategy, roadmap | Develop, iterate on, and share artifacts on a regular basis | 20% |
| Go to market | Build relationships with Sales team and create collateral to help with stage adoption | 10% |
| Building relationships with team | Coffee chats, company syncs, team meetings | 10% |
| Personal Development | Reading, Learning, Training | 5% |

## FY Q1 2023
| Theme | Notes | Percent |
| ------ | ------ | ------ |
| Sensing mechanisms / Discovery Track | Customer interviewing, competition, walkthroughs, KPIs | 30% |
| Build Track | Groom backlog, reach planning 2 milestones ahead | 25% |
| Vision, strategy, roadmap | Develop, iterate on, and share artifacts on a regular basis | 25% |
| Building relationships with team | Coffee chats, company syncs, team meetings | 15% |
| Personal Development | Reading, Learning, Training | 5% |

## FY Q4 2022
| Theme | Notes | Percent |
| ------ | ------ | ------ |
| Onboarding | PM and product onboarding, 100 day plan | 20% |
| Sensing mechanisms | Set up and automate customer interviewing, competition, walkthroughs | 40% |
| Vision, strategy, roadmap | Develop point of view, document, and share it | 30% |
| Building relationships with team | Coffee chats, company syncs, team meetings | 5% |
| Personal Development | Reading, Learning, Training | 5% |

## FY Q3 2022
| Theme | Notes | Percent |
| ------ | ------ | ------ |
| Onboarding | General onboarding, PM onboarding, 100 day plan | 100% |

</details>


# [Snippets](http://blog.idonethis.com/google-snippets-internal-tool/)

## Future
- Dedicated costs growth model
- Strategy OST
- Slack channel hygiene
- Dedicated feature docs and support. Start with frequently recently asked
- Dedicated for Government docs

## 2024-03-10
- Customer support (escalations, migrations)
- Discovery guidance for PMs to deploy to Dedicated
- DR scoping and documentation
- Roadmap planning and review
- PTO next week


## 2025-03-03
- Customer support (escalations, migrations)
- Hosted models discovery guidance
- DR scoping and documentation
- Dedicated Strategy and PI sync

## 2024-02-24
- Customer support (ref arch upgrades, escalations, migrations)
- Hosted models kickoff and next steps
- CKO watch party (Monday)
- SKO watch party (Wednesday)

## 2025-02-10
- Record Q1 kickoff
- Customer support/calls
- POV/trial scoping and proposal
- PTO 2025-02-14

## 2025-02-03
- Customer support (performance issues, escalations)
- Q1 planning
- Dedicated strategy and PI review sync

## 2025-01-27
- Deal support
- Customer support

## 2025-01-20
- Deal support
- Customer support
- Strategy update

## 2025-01-13
- Deal support
- Customer support
- FY26Q1 planning
- Strategy update
- Product discovery (ClickHouse, DR exercises)

## 2025-01-06
- Deal support
- Misc customer support
- FY26Q1 planning
- Async PI review
- Strategy update
- Product discovery (ClickHouse, DR exercises)

## 2024-12-16
- Deal support
- Roadmap planning and leadership review
- Dedicated and ClickHouse solution validation with C1
- Misc customer support
- OOO 2024-12-23 to 2025-01-03

## 2024-12-09
- Deal support
- Roadmap planning
- Dedicated and ClickHouse scoping

## 2024-11-25
- Customer escalations support
- Deal support
- Feature rollout support
- Onboarding brainstorm with Loryn
- OOO Thurs-Fri (US Holiday)

## 2024-11-18
- Deal support
- Q4 kickoff prep
- Customer feature rollout coordination
- Onboarding brainstorm with Loryn
- SME group support


## 2024-11-11
- Deal support
- Q4 planning

## 2024-11-04
- Deal support
- Audit support
- Q4 planning

## 2024-10-28
- Audit support
- Q3/Q4 deal support
- Finalize Q4 planning
- Misc customer support
- Deepa onboarding support

## 2024-10-21
- Catch up from PTO

## 2024-10-14
- PTO

## 2024-10-07
- PTO

## 2024-09-30
- GDG docs and direction page
- Field announcement
- Q4 roadmap planning
- Misc deal and customer support
- C8 customer call about RTO/RPO Wed
- Dedicated for Gov sponsor customer call
- Customer updates for Q3 features
- FedRAMP EM interviews

## 2024-09-23
- GDG docs and direction page
- Field announcement
- Q4 roadmap planning
- Misc deal and customer support

## 2024-09-16
- Margin analysis
- GDG customer support, docs, direction page
- GDG partner enablement session Tuesday
- Q4 roadmap planning
- Misc deal and customer support

## 2024-09-09
- C9 visit

## 2024-09-02
- Lely visit

## 2024-08-26
- PubSec roadmap planning and review
- GTM for Dedicated for Gov (direction page, docs, customer engagement plan)
- Deal support and pipeline review
- Dedicated group AMA
- (stretch) Dedicated costs growth model

## 2024-08-19
- PubSec roadmap planning and review
- C8 sizing, migrations, and performance support
- GTM for Dedicated for Gov (direction page, docs, customer engagement plan)
- Deal support and pipeline review
- Dedicated group AMA

## 2024-08-12
- 2 PM interviews
- Q3 kickoff recording
- GCP GTM alignment
- PubSec planning
- Deal support
- Document OKRs
- C1, C6 customer calls
- PI review

## 2024-08-05
- New customer kickoff and onboarding support
- Q3 kickoff recording
- GCP GTM alignment
- PubSec planning
- 2 PM interviews Thursday
- Deal support
- PTO Friday and next Monday

## 2024-07-29
- Prospect follow up questions
- Finalize Q3 plans
- Finalize emergency maintenance policy
- End of quarter deal support
- Review RFH issues and trends
- Interview PM candidate
- FedRAMP sponsor discovery 
- WAF rollout next steps


## 2024-07-22
- Prospect call Wed
- Q3 OKR/roadmap planning EA and PubSec
- Storage exploration and billing scoping
- PS enablement session
- Interview support Thursday

## 2024-07-15
- DWP visit Mon-Tues
- Q3 OKR/roadmap planning
- WAF communications
- Storage billing process

## 2024-07-08
- Dedicated deal support and customer calls
- Roadmap updates and scoping
- Start internal Dedicated office hours for field
- Holiday/PTO Thurs-Fri 

## 2024-07-01
- Deal support

## 2024-06-24
- Dedicated deal support and customer calls
- PM interviews

## 2024-06-17
- PTO

## 2024-06-10
- Dedicated deal support and customer calls
- FedRAMP support
- Roadmap planning

## 2024-06-03
- Customers calls (prospect, C1)
- FedRAMP support (marketing support)
- Token issue coordination / send customer comms by EOW
- Draft guidance about Dedicated for product groups

## 2024-05-27
- Dedicated deal support (many in flight)
- FedRAMP support (marketing materials review, NPI review)
- Token issue coordination / send customer comms by EOW
- New SKUs
- PI review

## 2024-05-20
- Dedicated deal support (many in flight)
- FedRAMP support (marketing materials review, sponsor next steps)
- Dedicated AMAs
- OKR review and refinement
- Token issue coordination

## 2024-05-13
- Dedicated deal support
- FedRAMP milestones alignment, sponsor call, and other support
- Kickoff Q2 Dedicated scope / roadmap / OKRs

## 2024-05-06
- Prepare and record Q2 kickoff

## 2024-04-29
- Roadmap/OKR planning

## 2024-04-22
- C6 post-cutover support and planning
- Plan C6 retro(s)
- FedRAMP support (sponsor call Tuesday)
- C7 and C8 escalations
- Kickoff Stable Sync
- Q2 OKR/roadmap planning and alignment
- PTO scheduled Fri-next Tues

## 2024-04-15
- C6 support
- C6 cutover Friday-Sunday

## 2024-04-08
- C6 Go / No-go 2 Monday
- C6 support
- FedRAMP support
- Google Cloud Next (async all week, except critical syncs)

## 2024-04-01
- C6 Go / No-go 1 Monday
- C6 support
- FedRAMP support
- Dedicated PM onboarding ([issue](https://gitlab.com/gitlab-com/Product/-/issues/13248))
- Misc Dedicated customer support (calls, requests for info)

## 2024-03-25
- Dedicated onboarding
- C6 support (perf testing, cutover plan, Rails config, SSH keys)
- FedRAMP support
- PI review

## 2024-03-18
- Catch up from Summit
- C6 support
- FedRAMP support
- Misc Dedicated support

## 2024-03-11
- Summit!

## 2024-03-04
- C6 migration support (cutover needs and plan, performance testing scoping)
- FedRAMP support (refining scope)

## 2024-02-26
- Monday PTO
- C6 migration support (cutover needs and plan, performance testing scoping)
- FedRAMP support (refining scope)
- Review of sales enablement materials for Dedicated ([epic](https://gitlab.com/gitlab-com/marketing/brand-product-marketing/product-marketing/-/issues/7445))

## 2024-02-19
- Monday Holiday
- C6 migration support (epic refinement, review network arch, cutover plan, performance testing scoping)
- FedRAMP support (refining scope)

## 2024-02-12
- Dedicated sandbox maintenance shift - DONE
- C6 migration support (finalize BYOD, cutover plan, performance testing scoping) - ongoing
- Strategy, PI review syncs - DONE
- FedRAMP support (refining scope, sponsor plan) - no progress

## 2024-02-05
- February milestone planning - Moving to quarterly
- C6 migration support (performance testing, cutover plan, unblocking)
- Dedicated customer calls - DONE
- FedRAMP PMO intake call - DONE
- OKR alignment - Moving to next week

## 2024-01-29
- C6 migration support (project plan, risks, unblocking) - In progress
- FedRAMP internal rekickoff - DONE
- OKR alignment - In progress

## 2024-01-22
- C6 pre-prod scoping
- C6 load testing scoping
- Dedicated customer call support - DONE
- Q1 planning - DONE
- FedRAMP internal rekickoff prep - In progress

## 2024-01-15
- C6 pre-prod scoping
- C6 container registry issue
- C6 load testing scoping
- C6 project management
- FedRAMP scope review
- Q1 planning

## 2024-01-08
- F&F Day Friday
- C6 pre-prod scoping
- C6 replication kickoff - DONE
- C6 load testing scoping
- FedRAMP scope review
- Start Q1 planning
- Facilitate PI review - DONE

## 2024-01-01
- Holiday/PTO Mon-Tues
- Catch up from holiday break - DONE
- Facilitate starting replication for C6 - DONE
- C6 pre-prod scoping - In progress

## 2023-12-18
- PTO Friday
- Dedicated C6 migration support and starting replication - Ready for replication, replication kickoff deferred until after new year
- Vulnerability SSOT
- SaaS Platforms KPI process
- January planning issue

## 2023-12-11
- Dedicated Support (C6 migration) - In progress
- Vulnerability SSOT - First iteration complete
- SaaS Platforms Mission review - DONE
- SaaS Platforms KPI process - In Progress

## 2023-12-04
- PTO Mon
- Advanced Search launch support (customer comms, etc)
- Dedicated Support (C6 migration)
- Vulnerability SSOT
- SaaS Platforms Mission review
- Saas Platforms North Star Workshop

## 2023-11-28
- PTO Thurs-next Mon
- Advanced Search launch support
- Dedicated Support (C6 migration)
- Vulnerability SSOT
- SaaS Platforms Mission review
- Saas Platforms North Star Workshop

## 2023-11-20
- Holiday OOO Thurs-Fri
- Dedicated Support
- Advanced Search launch support

## 2023-11-13
- Catch up from PTO
- Dedicated Support (Sales, onboarding, etc)
- Vulnerability SSOT
- Advanced Search launch support

## 2023-11-06
- PTO

## 2023-10-30
- PTO Wed-next week (coverage [issue](https://gitlab.com/gitlab-com/Product/-/issues/12827))
- Dedicated customer onboarding - Handed back to Loryn
- OKR and monthly planning - Monthly planning complete, Stephen will handle OKRs
- Vulnerability SSOT - Defer until after PTO
- Advanced Search open questions - Mostly answered, handed over to John

## 2023-10-23
- Talent Assessment - DONE
- Vulnerability SSOT
- Dedicated Support (customer call Thurs) - Customer complete. Some followups
- Finalize data analysis to support FY25 strategy planning. SaaS analysis complete.
- OKR and monthly planning
- Project plan - Drafted

## 2023-10-16
- Advanced Search open questions
- Vulnerability SSOT
- Dedicated Support
- Data analysis to support FY25 strategy planning

## 2023-10-09
- Catch up from OOO last week
- Vulnerability SSOT
- Dedicated support

## 2023-10-02
- OOO M-W Denver PM offsite
- F&F Friday
- Dedicated support
- Decision doc timelines

## 2023-09-25
- Oct milestone planning
- Sales support documentation
- Support Dedicated
- Epic cleaning up
- Decision doc feedback
- KPIs process
- Finish FedRAMP slides

## 2023-09-18
- F&F Fri
- Oct milestone planning - in progress
- Sales support documentation - in progress
- Remaining scope estimates - DONE
- Epic cleaning up - in progress

## 2023-09-11
- Advanced Search scoping
- Remaining scope estimates

## 2023-09-04
- Holiday Monday
- Agency sponsorship decision
- Customer call

## 2023-08-28
- Refine backlog of control and infra issues
- Sept milestone planning
- Follow up on agency sponsorship conversations
- PTO Wed-Fri

## 2023-08-21
- Refine backlog of control and infra issues - not started
- Draft Sept milestone planning - started
- Attending FedRAMP Headliner Summit (Wed) - DONE
- FedRAMP overview presentation - outline drafted
- Finalize initial KPIs - almost done

## 2023-08-14
- Finalize team milestone planning processes - DONE
- Start refining backlog of control and infra issues - Started
- Finish vulnerability scope MR - DONE

## 2023-08-07
- Team planning processes - In progress, have a proposal
- Vulnerability management writeup - pinged team for review
- Issue for SSL option - DONE
- POAM review - DONE

## 2023-07-31
- Overall solutions epic - DONE
- Vulnerability management writeup - In progress
- Project management iterations - DONE
- Grand Review coverage - DONE

## 2023-07-24

## 2023-07-17
- Catch up from PTO
- Customer calls

## 2023-06-26
- 16.1 RPM post-release duties (retro, handoff, https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/126153)
- Support UBI solution investigations
- Vulnerability management writeup
- Draft material on how to support customers implementing FedRAMP with SM
- US PubSec roadmap review discussions

## 2023-06-19
- Holiday Monday (but will be available for RPM duties)
- F&F Friday
- Finish 16.1 RPM duties
- UBI solution investigations

## 2023-06-12
- UBI solution investigations
- 16.1 RPM duties

## 2023-06-05
- 16.1 RPM duties
- AWS Summit DC (Wed-Thurs)
- UBI solution investigations
- Vuln management report

## 2023-05-30
- Holiday Monday
- UBI solution investigations
- Vuln management coffee chats

## 2023-05-23
- PTO Monday; F&F Friday
- UBI solution investigations
- Trainings

## 2023-05-15
- PTO Fri
- RAR feedback follow ups
- Vulnerability management documentation
- Project timeline

## 2023-05-08
- PTO Mon-Tues
- Catch up on RAR feedback follow ups
- Vulnerability management documentation

## 2023-05-01
- PTO

## 2023-04-24
- Pricing - In progress
- Review vuln remediation proposal - In progress
- Vulnerability burndown epic - In progress
- SAFE discussion - DONE

## 2023-04-17
- OOO Mon-Tues for Holiday and F&F
- Overall project timeline/roadmap - In progress
- Pricing - In progress
- Review vuln remediation proposal - In progress
- Vulnerability burndown epic - In progress
- MiSaaS opportunity canvas - In progress
- Deep dive on Infra work

## 2023-04-10
- Overall project timeline/roadmap - In progress
- Pricing - In progress
- Review vuln remediation proposal - In progress
- Vulnerability burndown epic - In progres
- MiSaaS opportunity canvas - In progress
- Deep dive on Infra work

## 2023-04-03
- Follow up customer call related to DAST FIPS compliance - DONE
- Overall project timeline/roadmap - In progress
- Pricing - In progress
- Review vuln remediation proposal - In progress
- Vulnerability burndown epic - In progres

## 2023-03-27
- Customer call related to DAST FIPS compliance - DONE
- Overall project timeline/roadmap - In progress
- Issue label hygiene and follow ups - In progress
- Pricing - In progress
- Review vuln remediation proposal

## 2023-03-20
- Out sick M-W
- Pricing estimate

## 2023-03-13
- Continue onboarding https://gitlab.com/gitlab-com/Product/-/issues/5507
- Finish transition issue
- Coffee chats
- FedRAMP sponsor discussion

## 2023-03-06
- Continue onboarding https://gitlab.com/gitlab-com/Product/-/issues/5507 - in progress
- Finish transition issue - in progress
- Coffee chats - DONE
- Sponsor questions - DONE
- Status update for https://gitlab.com/gitlab-org/gitlab/-/issues/388488 - DONE

## 2023-02-27
- USPubSec Onboarding and coffee chats
- Transition issue

## 2023-02-20
- RPIs
- Transition tasks
- Onboarding
- 2 Customer calls

## 2023-02-13
- Finish Q1 quarterly planning (draft all KRs, created video, team discussion)
- RPIs/Removals/Deprecations
- Finalize 15.10 planning
- Delivery direction
- Env + K8s Agent scoping and scheduling

## 2023-02-06
- Finish Q1 quarterly planning - OKRs drafted and moved into OKR items; some remaining wrap up items moving to next week
- 15.10 planning - Drafted
- RPIs/Removals/Deprecations - RPIs drafted
- Delivery direction - Reviewed
- Direction updates - DONE
- Collaborate on Category transfer/changes MRs - Meeting with Govern stage held
- Ramp on Env + K8s Agent design and help move scoping forward - Team discussion
- Outreach for customer interviews - Emails sent

## 2023-01-30
- Q1 quarterly planning (OKRs, projects, etc) - OKRs drafted
- Gartner questionnaire - Added mid-week; DONE
- 15.10 planning - in progress
- 15.9 Removals/Deprecations - no progress
- PMM activities - started blog post draft
- Outreach for customer interviews - little progress
- Merge FY24 investment plan - DONE
- Finish stage category review follow ups - DONE
- Ramp on Env + K8s Agent design and help move scoping forward - little progress

## 2023-01-23
- Ops PI Review - DONE
- Delivery Direction with Viktor - Sync meeting complete; will contribute async discussion
- Ramp customer interviews back up - No progress
- Respond to team feedback on FY24 investment plan and merge MR - Responded to all feedback, awaiting final approval to merge
- Product Marketing activities/planning - No progress
- Start FY24 OKR planning - Issue drafted and collaboration started with EM, UX
- Stage category review - All action items completed, awaiting Orit review

## 2023-01-16
- Holiday Monday
- RPIs - DONE
- Deprecation notices - DONE
- GigaOm survey and analyst call support - DONE
- Investment planning - MR approved by Viktor, shared with team for feedback

## 2023-01-09
- Catch up from PTO
- 15.9 milestone planning - DONE
- RPIs - Drafted

## 2022-12-19
- Urgent customer call with Solifi about Releases feature
- PI review - DONE
- Environment and K8s Agent coordination
- Feedback channel

## 2022-12-12
- Finalize 15.8 planning - DONE
- RPIs - DONE
- Environment and K8s Agent coordination - some progress
- PMM Q4 plan - DONE
- Feedback channel - no progress

## 2022-12-05
- 15.8 planning - almost done; draft shared and iterating
- Environment and K8s Agent coordination - some progress
- PMM Q4 plan - drafted

## 2022-11-14
- 15.7 planning
- Environment and K8s Agent coordination
- PI Review
- OKR and quarterly projects readout

## 2022-11-07
- Finalize OKR and quarterly projects planning - Incorporating feedback
- 15.7 planning - draft shared
- Interview shadowing - DONE
- Environment and K8s Agent coordination - not done

## 2022-10-31
- OKR planning and prior quarter review - first draft shared with team - DONE
- KubeCon debriefing - DONE
- Direction updates - DONE
- 15.7 Planning - In Progress, first draft created
- Self Assessment - DONE

## 2022-09-27

- Milestone planning, epic refinement
- Direction updates
- K8s Env research follow ups and feature
- Customer call about continuous verification
- Deploy approval feature guide

## 2022-08-08
- Milestone planning and kickoff video
- Onboarding experiment design
- Release post items
- Deployment approval competitor walkthrough
- Tag insights in Dovetail for Env/K8s Agent research

## 2022-08-01
- Milestone planning
- Direction updates
- Meetings to discuss features per customer request
- Socialize Q3 plan
- Make decision on how to update Release SMAU
- Tag insights in Dovetail for Env/K8s Agent research

## 2022-04-04
- Milestone planning
- Feature Flags investment case and opportunity canvas
- Feature Flags solutions investigation
- Problem Validation for Environments + GitLab k8s Agent
- Opportunity map for Release stage
- Reforge (week 3 of 5)

## 2022-03-28
- Milestone planning
- Feature Flags investment case and opportunity canvas
- Opportunity Mapping course (week 5 of 5)
- Reforge (week 2 of 5)
- OOO: PTO Friday

## 2022-03-07
- Milestone planning
- Removals and Deprecations
- Direction updates
- Opportunity Mapping course (week 2 of 5)

## 2022-02-28
- 14.10 planning
- Kickoff Problem Validation for Env and k8s agent
- Direction updates
- Opportunity Mapping course (week 1 of 5)

## 2022-02-14
- 14.9 Planning, kickoff video
- Issue planning breakdown
- RPIs
- Roadmap shareout

## 2022-02-07
- 14.9 Planning, kickoff video
- Issue planning breakdown
- Learn Feature Flags and do discovery customer call

## 2022-01-10
- Finalize milestone planning, kickoff video
- Group env research and scoping
- Review PIs
- Release capabilities demo

## 2022-01-03
- Milestone planning
- Group env research and scoping
- Direction updates
- OOO: F&F Monday

## 2021-12-27
- OOO for holidays

## 2021-12-20
- PI updates
- In-app notification brainstorm
- OOO: Wed-EOY

## 2021-12-13
- Milestone planning (14.7 and beyond)
- PI review
- Release capabilities demo
- OOO: Monday-Tuesday

## 2021-12-06
- Milestone planning
- Set up number of deployments metric in Sisense
- OOO: PTO Friday-Tuesday

## 2021-11-29
- Milestone planning
- Set up customer calls
- Start digging around Snowflake for data analysis
- Catch up on TODOs, Issues
- OOO: F&F Monday

## 2021-11-22
- Review SUS verbatims, summarize findings, and follow ups
- Release features deep dive
- Approval feature open questions
- OOO: Thursday, Friday (US Thanksgiving holiday), and Monday (F&F)

## 2021-11-15
- Contribute!
- Complete 14.5 RPIs
- Finalize 14.6 milestone planning and kickoff
- Feature direction share
- Backlog grooming
- Start recruiting customers for interviewing
- Approval feature open questions

## 2021-11-08
- Milestone planning and issue breakdown
- Backlog grooming
- Start recruiting customers for interviewing
- Approval feature open questions

## 2021-11-01
- Backlog grooming
- Future milestone planning and issue breakdown
- Direction updates
- Stage OKRs: Grade Q3 and Finalize Q4

## 2021-10-25
- Backlog and board cleanup
- Future milestone planning
- Q4 team OKR draft

## 2021-10-18
- Release PI's writeup with Kevin
- Continuous Interviewing course (week 5 of 5)
- Review triage reports and backlog cleanup
- Finish PM onboarding
- OOO: Friday and next Monday (PTO)

## 2021-10-11
- Product-specific onboarding
- 14.5 Milestone planning and breakdown with eng
- Release post items
- Continuous Interviewing course (week 4 of 5)
- OOO: Monday (Holiday), Friday (F&F day)

## 2021-10-04
- Product-specific onboarding
- Milestone planning and breakdown with eng
- Continuous Interviewing course (week 3 of 5)
- Notetaking for CAB

## 2021-09-27
- Onboarding (week 4)
- Product walkthrough
- Start looking at issues and metrics
- Continuous Interviewing course (week 2 of 5)

## 2021-09-20
- Onboarding (week 3)
- Attend team syncs
- Coffee chats
- Continuous Interviewing course (week 1 of 5)

## 2021-09-13
- Onboarding (week 2)
- Attend T-Mobile call as notetaker
- Coffee chats

## 2021-09-07
- Onboarding (week 1)
